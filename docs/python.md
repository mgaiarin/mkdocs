# Setup Python
## Setup repo

Per far funzionare il sistema è mandatorio l'utilizzo di Python.  
Per automatizzare tutti i pacchetti necessari a Python, la cosa migliore probabilmente è l'utilizzo di [pipenv](https://pypi.org/project/pipenv/). Da valutare anche l'uso di `venv` e `requirements` visto che `pipenv` richiede una versione di python3.6 o superiore, e magari questo potrebbe creare dei problemi di compatibilità (da valutare).

Setup per mantainer:

1. Installare mkdocs: `pipenv install mkdocs`
2. Plugin utili:
  1. [Liste](https://pypi.org/project/mdx-truly-sane-lists/): `pipenv install mdx-truly-sane-lists`.
    1. Questa estensione permette l'utilizzo di 4 livelli di indentazione nelle liste.
    2. Per distinguere ogni livello bisogna usare 2 spazi
  2. [Configurazione pagine](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin): `pipenv install mkdocs-awesome-pages-plugin`.
    1. **Nota:** Affinchè le pagine vengano mostrate correttamente nel menù laterale sotto al titolo della pagina (contraddistinto dal singolo #) non deve esserci del testo. Creare subito un header di secondo livello. Ovvero bisogna avere qualcosa del tipo:
      ```
      # Titolo
      <zona vuota>
      ## Sotto titolo
      testo qualsiasi...
      ```
     2. Se non si fa così le pagine non verranno listate nel menù.
  3. [Link tra pagine](https://github.com/orbikm/mkdocs-ezlinks-plugin): `pipenv install mkdocs-ezlinks-plugin`
  4. [Possibile tema](https://squidfunk.github.io/mkdocs-material/):`pipenv install mkdocs-material`
    3. Maggiori informazioni sul tema nella pagina di setup di [mkdocs](mkdocs.md)
3. Una volta installati tutti i pacchetti Python necessari creare il `Pipfile.lock` con il comando `pipenv lock`.

## Utilizzo

Per iniziare ad usare una pagina di documentazione:

1. Assicurarsi di avere installato Python3.8
2. Assicurarsi di avere installato `pipenv` ([ref](https://pypi.org/project/pipenv/))
3. Clonare il repo
4. Installare le dipendenze Python eseguendo: `pipenv sync`
5. Editare/aggiungere file documentazione markdown
6. Verificare le modifiche prima del push su localhost: `pipenv run mkdocs serve -a localhost:9000`
   1. Su browser andare al link `localhost:9000`

**Nota:** La CI automaticamente fa il deploy sulle pages di gitlab, quindi basta fare l'update dei markdown e il link verrà aggiornato automaticamente. Per questo motivo chi non ha bisogno di una preview in locale potrebbe anche evitare il setup di python ed editare solo la documentazione.