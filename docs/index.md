# Home

Raccolta della documentazione per impostare un repo mkdocs. Il repo sviluppato può essere usato come template per i futuri progetti dopo le necessarie modifiche.  
Questo repo è stato fatto per testare il sistema e spiegare come impostare il lavoro. Di seguito sono linkate le pagine con informazioni generali per il setup del sito statico integrato con GitLab.  
Il lavoro svolto qui può essere preso come punto di partenza per andare a definire un template definitivo.

## Pagine principali

1. [Setup GitLab](gitlab.md)
2. [Setup python](python.md)
3. [Setup mkdocs](mkdocs.md)
