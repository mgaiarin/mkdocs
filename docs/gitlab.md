# Setup GitLab
## Intro
L'idea è quella di sfruttare mkdocs per generare una wiki interattiva della documentazione dei repository.  
GitLab permette di creare un sito statico di un repository, fornendo supporto a diversi tool per la generazione delle pagine (es. Jekill, Hexo, Hugo, mkdocs, ecc.). Inoltre fonisce gratuitamente un dominio di default o è possibile usarne uno privato. Si possono trovare più informazioni sulle pagine dedicate [ https://pages.gitlab.io]( https://pages.gitlab.io) e dalla documentazione ufficiale [https://docs.gitlab.com/ce/user/project/pages/](https://docs.gitlab.com/ce/user/project/pages/)

## Setup

La pagina statica del sito viene buildata dalla [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/), seguendo le istruzioni definite nel file `.gitlab-ci.ylm`.  
Per preparare il repo:

1. Aggiungere i file di configurazione di Python, vedi [qui](python.md) (se si usa un template dovrebbero essere già presenti).
2. Creare la documentazione mkdocs come definito nella pagina dedicata, vedi [qui](mkdocs.md)
3. Configurare la CI tramite il file `.gitlab-ci.ylm`.
   1. Il file di configurazione usato per questo repo può essere già usato come template.
   2. Definisce la versione di python da usare.
   3. Definisce le operazioni da fare prima di avviare un job -> in questo caso installa `pipenv` e poi fa il `sync` dei pacchetti.
   4. Definisce i job per fare il deploy del sito
4. Se tutto va a buon fine andare su `repository -> settings -> pages`
   1. In questa pagina verrà mostrato il link per accedere al sito.