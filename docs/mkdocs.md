# Setup mkdocs

## Intro
mkdocs è un generatore di siti statici utile in particolare per la documentazione di progetti, [pagina ufficiale](https://www.mkdocs.org/).  

## Setup
Setup per usare mkdocs in maniera base e avere un'idea di come funziona.  
Steps per usare mkdocs:

1. Installazione: `pip install mkdocs`
2. Creazione di un nuovo progetto: `mkdocs new my-project`
  1. Questo comando crea la cartella inizializzata
  2. Ci sarà il file di configurazione `mkdocs.ylm`
  3. Ci sarà la cartella `docs` con il file `index.md`
  4. Aggiungere nuove pagine markdown all'interno della cartella `docs`
  5. Verificare il sito su localhost: `mkdocs serve -a localhost:9000`
3. A questo punto editando il file `mkdocs.ylm` è possibile editare varie impostazioni grafiche.
  6. Si possono aggiungere la navbar, temi e altro.
  7. Ci sono dei temi di default, oppure si possono usare temi di terze parti come [material](https://squidfunk.github.io/mkdocs-material/).
    1. Questo è il tema usato per questo esempio
    2. Offre varie personalizzazioni e estensioni Python.
    3. Offre una buona interfaccia utente
    4. Sfrutta molte utilities esterne, [setup vari](https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/).
    5. Sfrutta varie [estensioni Python](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown-extensions/)
