# Documentazione MKDOCS

Documentazione per fare un setup minimale di mkdocs.

Istruzioni:

1. Clonare il repo
2. Installare Python 3.9+
3. Installare `pipenv` con `sudo apt install pipenv`
4. Installare le dipendenze con `pipenv sync`
5. Eseguire il server locale HTML `pipenv run mkdocs serve -a localhost:9000`
6. Per pubblicare le modifiche online basta fare il push della documentazione. 
   1. La pages di gitlab si aggiorna automaticamente grazie al CI.
   2. La pagina si trova al seguente [link](https://mgaiarin.gitlab.io/mkdocs/#pagine-principali)
